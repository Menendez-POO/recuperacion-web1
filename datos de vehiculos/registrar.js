function vehiculos() {
    // Hacemos uso de expresiones regulares
    const expresiones_codigo = /^[0-9]{4}$/;
    const expresiones_descripcion = /^[a-zA-Z]{3,20}$/;
    const expresiones_marca = /^[a-zA-Z]{3,20}$/;
    const expresiones_modelo = /^[a-zA-Z]{3,20}$/;
    const expresiones_año = /^[0-9]{4}$/;

    // Obtenemos los valores de los campos con su id respectivo
    const codigo = document.getElementById("codigo");
    const descripcion = document.getElementById("descripcion");
    const marca = document.getElementById("marca");
    const modelo = document.getElementById("modelo");
    const año = document.getElementById("año");

    // validamos los datos antes de guardarlos en la base de datos
    if (codigo == "" || descripcion == "" || marca == "" || modelo == "" || año == "") {
        alert("Los campos estan vacios");
    } else if (!expresiones_codigo.test(codigo)) {
        alert("El codigo debe tener 4 digitos");
    } else if (!expresiones_descripcion.test(descripcion)) {
        alert("La descripcion debe tener 3 a 20 caracteres");
    } else if (!expresiones_marca.test(marca)) {
        alert("La marca debe tener 3 a 20 caracteres");
    } else if (!expresiones_modelo.test(modelo)) {
        alert("El modelo debe tener 3 a 20 caracteres");
    } else if (!expresiones_año.test(año)) {
        alert("El año debe tener 4 digitos");
    } else {
        // Creamos un array la cual contendra los datos de los campos
        const DatosVehiculos = {
            codigo: $('#codigo').val(),
            descripcion: $('#descripcion').val(),
            marca: $('#marca').val(),
            modelo: $('#modelo').val(),
            año: $('#año').val(),
        };
        // enviamos nuestros datos por el mètodo post, para aquello hicimos uso de una ruta(api)
        // llamada vehiculos
        $.post('/vehiculos', DatosVehiculos, function(data) {
            alert(`Los datos se enviaron correctamente ${data}`);
        });
    }
}
