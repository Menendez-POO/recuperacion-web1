function suma() {
    const cedula = document.getElementById("cedula");
    let suma = 0;
    let sumaPares = 0;
    let sumaImpares = 0;
    let numero = cedula.value;
    for (let i = 0; i < numero.length; i++) {
        suma += parseInt(numero[i]);
    }
    for (let i = 0; i < numero.length; i++) {
        if (parseInt(numero[i]) % 2 == 0) {
            sumaPares += parseInt(numero[i]);
        } else {
            sumaImpares += parseInt(numero[i]);
        }
    }
    alert("La suma de los numeros es: " + suma + " La suma de los numeros pares es: " + sumaPares + " La suma de los numeros impares es: " + sumaImpares);
}
